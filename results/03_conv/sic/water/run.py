from copy import copy
from pyscf import scf
from pyflosic2.io.flosic_io import write_xyz
from pyflosic2.io.uflosic_io import read_flosic_xyz
from pyflosic2.time.timeit import timeit
from pyflosic2.parameters.flosic_parameters import parameters
from pyflosic2.sic.uflosic import UFLOSIC, ufodopt
from pyflosic2.sic.uflo import UFLO
from pyflosic2.parameters.flosic_parameters import set_grid
import numpy

def run(f_xyz,basis,grid):
    """
        run 
        Run a FLO-SIC calculation for f_xyz 
        with basis and grid. 

        Note: Used fixed "LDA,PW" 
    """
    # Standard parameters
    p = parameters(mode='unrestricted')

    # Computational parameter
    p.xc = 'LDA,PW'  # r2SCAN '497,498'
    p.verbose = 3
    p.conv_tol = 1e-8
    p.use_analytical_fforce = True
    p.opt_method = 'L-BFGS-B'
    p.basis = basis
    p.grid_level = grid

    # System information
    atoms, sym1, sym2 = read_flosic_xyz(f_xyz)
    p.init_atoms(atoms)

    p0 = copy(p)

    # DFT
    def dft(p):
        p.log.header('Task: DFT (UKS)')
        mf = scf.UKS(p.mol)
        # SS: b/c different logs
        # SS: PySCF has a different logger
        mf.verbose = 0
        mf.xc = p.xc
        mf.conv_tol = p.conv_tol
        mf = set_grid(mf,value=p.grid_level)
        mf.grids.prune = None
        edft = mf.kernel()
        dip = mf.dip_moment()
        return p, mf, edft

    p, mf, edft = dft(p)
    mf0 = copy(mf)

    def flosic(p, mf):
        """
            Density matrix (DM) for inital fixed Fermi orbital descriptors (FODs)
            DM : optimized
            FODs: not optimized
        """
        p.log.header('Task: FLO-SIC')
        p.optimize_FODs = False
        mflo = UFLOSIC(mf=mf, p=p)
        etot = mflo.kernel()
        dip  = mflo.dip
        # SS: to be sure we do not influence other calcs
        del mflo
        return etot, dip

    etot, dip = flosic(p, mf)
    dm = numpy.linalg.norm(dip)
    # SS: to see the real performance we do not want to use
    # SS: information from previous steps
    mf = copy(mf0)
    p = copy(p0)

    return etot, dm

def main(f_xyz="initial_h2o.xyz"): 
    """
        main 
        Main function to test this routine.
    """
    f_file = open('data.dat','w',buffering=1)
    Radial = [50,100,150,200,250,300]
    Angular = [110, 170, 302, 590, 1202, 1454]
    Basis = ["pc0","aug-pc0","unc-aug-pc0","pc1","aug-pc1","unc-aug-pc1","pc2","aug-pc2","unc-aug-pc2","pc3","aug-pc3","unc-aug-pc3","pc4","aug-pc4","unc-aug-pc4"]
    for bas in Basis:
        for rad in Radial:
            for ang in Angular:
                grid = (rad,ang)
                etot, dm = run(f_xyz=f_xyz,
                               basis=bas,
                               grid=grid)
                f_file.write(f'{bas} {rad:4d} {ang:4d} {etot:13.8f} {dm:10.4f}\n')
    f_file.close() 
        
if __name__ == '__main__':
    main(f_xyz="initial_h2o.xyz")
