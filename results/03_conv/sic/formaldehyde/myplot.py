import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.ticker as tick
from  matplotlib import rc
# Overwrite some matplotlib variables globally 
font = {'family' : 'sans',
        'weight' : 'normal',
        'size'   : 20}

rc('font', **font)
rc('lines', linewidth=3,markersize=22)


def x_fmt(x, y):
    """
        x_fmt
        -----
        Used to format the x labels.
    """
    return f'{x:2.0f}'

def y_fmt(x, y):
    """
        y_fmt
        -----
        Used to format the y labels.
    """
    if x != 0.0:
        return f'{x:1.0e}'
    else:
        return '0'


def read_data(f_file,basis_fam,unc,aug,rad,ang):
    """
        read_data
        Read data from f_file and extract values.
    """
    f = open(f_file,"r") 
    ll = f.readlines()
    f.close() 
    NAME = []
    RAD = []
    ANG = []
    E = []
    DM = []
    for i,l in enumerate(ll): 
        if l.find(basis_fam) != -1 and l.find("unc") != unc and l.find("aug") != aug:
            e_name, e_rad, e_ang, e_etot, e_dm = l.split() 
            e_rad = int(e_rad) 
            e_ang = int(e_ang) 
            e_etot = float(e_etot)
            e_dm = float(e_dm)
            if e_rad == rad and e_ang == ang: 
                NAME.append(e_name) 
                RAD.append(e_rad) 
                ANG.append(e_ang) 
                E.append(e_etot) 
                DM.append(e_dm) 
                print(e_name,e_rad,e_ang,e_etot,e_dm)
    E = np.array(E)
    DM = np.array(DM)
    return NAME, RAD, ANG, E, DM

def get_rad_basis(f_file,basis_fam,unc,aug,ang):
    """
        get_rad_basis
        Get radial values for fixed ang. 
    """
    f = open(f_file,"r")
    ll = f.readlines()
    f.close()
    NAME = []
    RAD = []
    ANG = []
    E = []
    DM = []
    for i,l in enumerate(ll):
        if l.find(basis_fam) != -1 and l.find("unc") != unc and l.find("aug") != aug:
            e_name, e_rad, e_ang, e_etot, e_dm = l.split()
            e_rad = int(e_rad)
            e_ang = int(e_ang)
            e_etot = float(e_etot)
            e_dm = float(e_dm)
            if e_ang == ang:
                NAME.append(e_name)
                RAD.append(e_rad)
                ANG.append(e_ang)
                E.append(e_etot)
                DM.append(e_dm)
                print(e_name,e_rad,e_ang,e_etot,e_dm)
    E = np.array(E)
    DM = np.array(DM)
    return NAME, RAD, ANG, E, DM 

def get_ang_basis(f_file,basis_fam,unc,aug,rad):
    """
        get_ang_basis
        Get angular values for fixed rad. 
    """
    f = open(f_file,"r")
    ll = f.readlines()
    f.close()
    NAME = []
    RAD = []
    ANG = []
    E = []
    DM = []
    ALF = []
    for i,l in enumerate(ll):
        if l.find(basis_fam) != -1 and l.find("unc") != unc and l.find("aug") != aug:
            e_name, e_rad, e_ang, e_etot, e_dm = l.split()
            e_rad = int(e_rad)
            e_ang = int(e_ang)
            e_etot = float(e_etot)
            e_dm = float(e_dm)
            if e_rad == rad:
                NAME.append(e_name)
                RAD.append(e_rad)
                ANG.append(e_ang)
                E.append(e_etot)
                DM.append(e_dm)
                print(e_name,e_rad,e_ang,e_etot,e_dm)
    E = np.array(E)
    DM = np.array(DM)
    return NAME, RAD, ANG, E, DM


def main(f_file="data.dat",grid_only=False,basis_only=True):
    """
        main 
        Main function to test this routine. 
    """
    # pc-n
    basis_fam = "pc"
    unc = 0
    aug = 0
    Radial = [50,100,150,200,250,300]
    Angular = [110, 170, 302, 590, 1202, 1454]
    rad = 200  
    ang = 1454 
    pc_n = read_data(f_file=f_file,
                     basis_fam=basis_fam,
                     unc=unc,
                     aug=aug,
                     rad=rad,
                     ang=ang)

    # aug-pc-n
    basis_fam = "pc"
    unc = 0
    aug = -1
    Radial = [50,100,150,200,250,300]
    Angular = [110, 170, 302, 590, 1202, 1454]
    rad = 200  
    ang = 1454 
    aug_pc_n = read_data(f_file=f_file,
                     basis_fam=basis_fam,
                     unc=unc,
                     aug=aug,
                     rad=rad,
                     ang=ang)

    # unc-aug-pc-n
    basis_fam = "pc"
    unc = -1 
    aug = -1
    Radial = [50,100,150,200,250,300]
    Angular = [110, 170, 302, 590, 1202, 1454]
    rad = 200   
    ang = 1454 
    unc_aug_pc_n = read_data(f_file=f_file,
                     basis_fam=basis_fam,
                     unc=unc,
                     aug=aug,
                     rad=rad,
                     ang=ang)
    
    # x axis for pc-n basis sets -> n
    n = [0,1,2,3,4]
    fig1, ax = plt.subplots(3,2)
    loc = 1 
    
    # rad 
    pc3_rad = get_rad_basis(f_file=f_file,
                             basis_fam="pc3",
                             unc=0,
                             aug=0,
                             ang=ang)
    aug_pc3_rad = get_rad_basis(f_file=f_file,
                            basis_fam="aug-pc3",
                            unc=0,
                            aug=-1,
                            ang=ang)
    unc_aug_pc3_rad = get_rad_basis(f_file=f_file,
                            basis_fam="unc-aug-pc3",
                            unc=-1,
                            aug=-1,
                            ang=ang)


    # Etot 
    ax[0,0].plot(pc3_rad[1],pc3_rad[3]-pc3_rad[3][-1],"s-",label="pc-3")
    ax[0,0].plot(aug_pc3_rad[1],aug_pc3_rad[3]-aug_pc3_rad[3][-1],"p-",label="aug-pc-3")
    ax[0,0].plot(unc_aug_pc3_rad[1],unc_aug_pc3_rad[3]-unc_aug_pc3_rad[3][-1],".-",label="unc-aug-pc-3")
    ax[0,0].set_xlabel(r"grid($N_{\mathrm{rad}},$"+f"{ang})")
    ax[0,0].set_ylabel(r"$\Delta E_{\mathrm{tot}}$ [$E_{\mathrm{h}}$]")
    ax[0,0].legend(loc=loc)
    # DM
    ax[0,1].plot(pc3_rad[1],pc3_rad[4]-pc3_rad[4][-1],"s-",label="pc-3")
    ax[0,1].plot(aug_pc3_rad[1],aug_pc3_rad[4]-aug_pc3_rad[4][-1],"p-",label="aug-pc-3")
    ax[0,1].plot(unc_aug_pc3_rad[1],unc_aug_pc3_rad[4]-unc_aug_pc3_rad[4][-1],".-",label="unc-aug-pc-3")
    ax[0,1].set_xlabel(r"grid($N_{\mathrm{rad}},$"+f"{ang})")
    ax[0,1].set_ylabel(r"$\Delta \mu$ [D]")



    # ang
    pc3_ang = get_ang_basis(f_file=f_file,
                             basis_fam="pc3",
                             unc=0,
                             aug=0,
                             rad=rad)
    aug_pc3_ang = get_ang_basis(f_file=f_file,
                            basis_fam="aug-pc3",
                            unc=0,
                            aug=-1,
                            rad=rad)
    unc_aug_pc3_ang = get_ang_basis(f_file=f_file,
                            basis_fam="unc-aug-pc3",
                            unc=-1,
                            aug=-1,
                            rad=rad)


    # Etot 
    ax[1,0].plot(pc3_ang[2],pc3_ang[3]-pc3_ang[3][-1],"s-",label="pc-3")
    ax[1,0].plot(aug_pc3_ang[2],aug_pc3_ang[3]-aug_pc3_ang[3][-1],"p-",label="aug-pc-3")
    ax[1,0].plot(unc_aug_pc3_ang[2],unc_aug_pc3_ang[3]-unc_aug_pc3_ang[3][-1],".-",label="unc-aug-pc-3")
    ax[1,0].set_xlabel(r"grid("+f"{rad}"+r"$,N_{\mathrm{ang}}$)")
    ax[1,0].set_ylabel(r"$\Delta E_{\mathrm{tot}}$ [$E_{\mathrm{h}}$]")
    ax[1,0].set_xticks(pc3_ang[2])
    ax[1,0].set_xticklabels(pc3_ang[2],rotation=79)
    # DM 
    ax[1,1].plot(pc3_ang[2],pc3_ang[4]-pc3_ang[4][-1],"s-",label="pc-3")
    ax[1,1].plot(aug_pc3_ang[2],aug_pc3_ang[4]-aug_pc3_ang[4][-1],"p-",label="aug-pc-3")
    ax[1,1].plot(unc_aug_pc3_ang[2],unc_aug_pc3_ang[4]-unc_aug_pc3_ang[4][-1],".-",label="unc-aug-pc-3")
    ax[1,1].set_xlabel(r"grid("+f"{rad}"+r"$,N_{\mathrm{ang}}$)")
    ax[1,1].set_ylabel(r"$\Delta \mu$ [D]")
    ax[1,1].set_xticks(pc3_ang[2])
    ax[1,1].set_xticklabels(pc3_ang[2],rotation=79)



    # total 
    # Etot 
    ax[2,0].plot(n,pc_n[3]-pc_n[3][-1],"s-",color="brown",label="pc-n")
    ax[2,0].plot(n,aug_pc_n[3]-aug_pc_n[3][-1],"p-",color="silver",label="aug-pc-n")
    ax[2,0].plot(n,unc_aug_pc_n[3]-unc_aug_pc_n[3][-1],".-",color="gold",label="unc-aug-pc-n")
    ax[2,0].set_xticks(n)
    ax[2,0].set_xlabel(r"$n$"+f"@grid({rad},{ang})")
    ax[2,0].set_ylabel(r"$\Delta E_{\mathrm{tot}}$ [$E_{\mathrm{h}}$]")
    ax[2,0].legend(loc=loc)
    # DM 
    ax[2,1].plot(n,pc_n[4]-pc_n[4][-1],"s-",color="brown",label="pc-n")
    ax[2,1].plot(n,aug_pc_n[4]-aug_pc_n[4][-1],"p-",color="silver",label="aug-pc-n")
    ax[2,1].plot(n,unc_aug_pc_n[4]-unc_aug_pc_n[4][-1],".-",color="gold",label="unc-aug-pc-n") 
    ax[2,1].set_xticks(n)
    ax[2,1].set_xlabel(r"$n$"+f"@grid({rad},{ang})") 
    ax[2,1].set_ylabel(r"$\Delta \mu$ [D]") 

    ax[0,0].yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
    ax[0,1].yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
    ax[1,0].yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
    ax[1,1].yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
    ax[2,0].yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
    ax[2,1].yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))

    if basis_only: 
        ax[0,0].clear()
        ax[0,1].clear()
        ax[0,0].axis('off')
        ax[0,1].axis('off')
        #
        ax[1,0].clear()
        ax[1,1].clear()
        ax[1,0].axis('off')
        ax[1,1].axis('off')

    if grid_only:
        ax[2,0].clear()
        ax[2,1].clear()
        ax[2,0].axis('off')
        ax[2,1].axis('off')


    # Adjust subplots
    plt.subplots_adjust(left=0.0695,
                        bottom=0.1,
                        right=0.99,
                        top=0.95,
                        wspace=0.25,
                        hspace=0.676)

    plt.show()


if __name__ == "__main__": 
    main(f_file="data.dat",grid_only=False,basis_only=True)
    main(f_file="data.dat",grid_only=True,basis_only=False) 
