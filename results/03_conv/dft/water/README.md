- This procedure is used to generate Fig. 3 and Fig. SF1 (a)  
- 1st: run the python script
  ```bash 
  python3 run.py
  ```
  to generate the data.
- 2nd: run the python script    
  ```bash 
  python3 myplot.py
  ```
  to plot the data.
- 3rd: trim the white space of the figures using convert
  ```bash 
  convert -trim NAME.png NAME.png
  ```
- Note: We made a backup of the data called data_ref.dat
