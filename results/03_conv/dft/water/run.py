import numpy as np
from pyscf import gto, dft
from dippo import pkg_path

def run(f_name,spin=0,charge=0,basis='pc2',xc='LDA,PW',grid=(150,1202)):
    """
        run 
    """
    prefix = f_name.split('.')[0]
    # DFT 
    mol = gto.M(atom=f_name, basis=basis,spin=spin,charge=charge)
    mf = dft.UKS(mol)
    mf.verbose = 0
    mf.max_cycle = 300
    mf.conv_tol = 1e-8
    mf.xc = xc
    mf.grids.atom_grid = grid
    etot = mf.kernel()
    dm_i = mf.dip_moment(verbose=0)
    dm = np.linalg.norm(dm_i)
    alpha_ij = mf.Polarizability().polarizability()
    alpha = np.trace(alpha_ij)/3.

    return etot, dm, alpha

def main(xc='LDA,PW'):
    """
        main 
    """
    f_out = "data.dat"
    f_name = f"{pkg_path}/structures/CCCBDB/water_CCCBDB.xyz"
    Radial = [50,100,150,200,250,300]
    Angular = [110, 170, 302, 590, 1202, 1454]
    Basis = ["pc0","aug-pc0","unc-aug-pc0","pc1","aug-pc1","unc-aug-pc1","pc2","aug-pc2","unc-aug-pc2","pc3","aug-pc3","unc-aug-pc3","pc4","aug-pc4","unc-aug-pc4"]

    f = open(f_out,'w',buffering=1)

    for bas in Basis:
        for rad in Radial:
            for ang in Angular:
                grid = (rad,ang)
                etot, dm, alpha = run(f_name,spin=0,charge=0,basis=bas,xc=xc,grid=grid)
                f.write(f"{bas} {rad:4d} {ang:4d} {etot:13.8f} {dm:10.4f} {alpha:10.4f}\n")
    f.close()

if __name__ == "__main__":
    main(xc="LDA,PW")

