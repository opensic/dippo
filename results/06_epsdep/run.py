import numpy as np 
from pyscf import scf, gto
from pyflosic2 import parameters, Atoms 
from pyflosic2.io.uflosic_io import atoms2flosic as uatoms2flosic, atoms2pyscf as uatoms2pyscf
from pyflosic2.utils.flosic_utils import apply_electric_field
from pyflosic2.time.timeit import tictoc 
from copy import copy 
from dippo.db import key2latex
from dippo.centering import center_by_moments_of_inertia

# These are properties of the class (cls)
# They can be used with making an instance
# SS: Note the tiers are here only for convenience
# SS: the grid was pre-determined with indivdual grid convergence tests 

#tier1 = {'basis': 'aug-pc-0', 'grid_level': (200,1454)}
#tier2 = {'basis': 'aug-pc-1', 'grid_level': (200,1454)}
#tier3 = {'basis': 'aug-pc-2', 'grid_level': (200,1454)}
#tier4 = {'basis': 'aug-pc-3', 'grid_level': (200,1454)}
#tier5 = {'basis': 'aug-pc-4', 'grid_level': (200,1454)}

# Danger: Testing purposes only!
# Use to produce fast values. 
tier1 = {'basis': 'pc-0', 'grid_level': (100,110)}
tier2 = {'basis': 'pc-1', 'grid_level': (100,110)}
tier3 = {'basis': 'pc-2', 'grid_level': (100,110)}
tier4 = {'basis': 'pc-3', 'grid_level': (100,110)}
tier5 = {'basis': 'pc-4', 'grid_level': (100,110)}


# Define more tiers
tiers = {'tier1': tier1,
         'tier2': tier2,
         'tier3': tier3,
         'tier4': tier4,
         'tier5': tier5}


def set_grid(mf, value, prune=None):
    """
        set_grid
        --------
        PyFLOSIC2 handling of the PySCF grid. 
    """
    if isinstance(value, int):
        mf.grids.level = value
    if isinstance(value, tuple):
        mf.grids.atom_grid = value
    if not isinstance(value, tuple) and not isinstance(value, int):
        print(TypeError('grid needs to be a tuple (e.g., (30,302)) or an integer (0-9).'))
    return mf


class Results: 
    def __init__(self,key): 
        self.key = key 

class Benchmark:
    def __init__(self,name=None,systems_dct=None,kernel=None,tier_name="tier1",eps=0.0000001): 
        self.name = name 
        self.systems_dct = systems_dct
        self._kernel = kernel 
        self.tier_name = tier_name 
        self.eps = eps 
        self._results = {}

    def _show(self):
        header = tiers[self.tier_name]
        print(f"eps: {self.eps} basis: {header['basis']} grid: {header['grid_level']}")
        for key, value in self._results.items():
            print(f"{self.name:>10} & {key2latex[key]:>20} & {value.dm: 10.2f} & {value.dm_fd: 10.2f} & {value.alpha: 10.2f} & {value.alpha_fd: 10.2f} \\ \\\\")

    def kernel(self):
        # key,value = name, atoms 
        for key,value in self.systems_dct.items(): 
            res = self._kernel(key,value,eps=self.eps,tier_name=self.tier_name)
            res_dct = {key: res}
            self._results.update(res_dct) 
        print(self._results)  
        self._show() 

def init_nuclei(p,atoms):
    """
        init_nuclei
        This is a hack for init_atoms if the atoms only contain nuclei 
    """
    p.nuclei = atoms 
    p.mol = gto.M(atom=uatoms2pyscf(p.nuclei), basis=p.basis, spin=p.spin, charge=p.charge, symmetry=p.symmetry)
    return p 

def calc(key,atoms,eps=0.0000001,tier_name="tier1"):
    """
        calc 
        Calculate dipole moment using analytical and finite 
        difference approach.

        Input 
            - started script from 
                pyflosic_dev/pyflosic_dev/test/knight_valley/uflosic/test_uflosic.py
    """
    # Center the atoms by inertia tensor 
    atoms = center_by_moments_of_inertia(atoms) 

    # Standard parameters
    p = parameters(mode='unrestricted', log_name='UFLOSIC.log')

    # Computational parameter
    p.xc = "LDA,PW" 
    my_tier = tiers[tier_name]
    p.basis = my_tier["basis"]
    p.grid_level = my_tier["grid_level"] 
    p.verbose = 3
    p.conv_tol = 1e-8
    p.use_analytical_fforce = True
    p.opt_method = 'CG'

    # System information
    #p.init_atoms(atoms)
    # SS: For testing reasons nuclei/DFT only. 
    p = init_nuclei(p,atoms)
    #write_xyz(p.atoms)

    p0 = copy(p)

    # DFT
    @tictoc(p)
    def dft(p):
        """
            dft 
            Calculate ground state DFT

            Properties 
                - energy 
                - dipole moment 
                - polarizability 
        """
        mf = scf.UKS(p.mol)
        # SS: b/c different logs
        # SS: PySCF has a different logger
        mf.verbose = 0
        mf.xc = p.xc
        mf.conv_tol = p.conv_tol
        #mf.grids.level = p.grid_level
        mf = set_grid(mf,p.grid_level) 
        edft = mf.kernel()
        dm = mf.dip_moment(verbose=0)
        alpha_ij = mf.Polarizability().polarizability()
        alpha = np.trace(alpha_ij)/3.
        return p, mf, edft, dm, alpha_ij, alpha  

    @tictoc(p)
    def dft_efield(p,efield):
        """
            dft_efield 
            Calculate DFT with an additional efield 

            Properties
                - energy 
                - dipole moment 
                - polarizability 
        """
        mf = scf.UKS(p.mol)
        apply_electric_field(mf,efield)
        # SS: b/c different logs
        # SS: PySCF has a different logger
        mf.verbose = 0
        mf.xc = p.xc
        mf.conv_tol = p.conv_tol
        mf = set_grid(mf,p.grid_level)
        edft = mf.kernel()
        dm = mf.dip_moment(verbose=0)
        return p, mf, edft, dm
    
    # Calculate ground state and corresponding dipole 
    p, mf, edft, dm_i, alpha_ij, alpha = dft(p)
   
    # Directions 
    Vec = [np.array([1,0,0]),np.array([0,1,0]),np.array([0,0,1])]
    dm_i_fd = np.empty(3)
    alpha_ij_fd = np.empty((3,3)) 
    # Calculate dipole components/polarizeability components using FD 
    for i,vec in enumerate(Vec):
        efield_plus = vec*eps
        efield_minus = -vec*eps

        # Dipole a.u. to Debye 
        unit = 2.54174821442

        p, mf, edft_plus, dm_plus = dft_efield(p,efield_plus)

        p, mf, edft_minus, dm_minus = dft_efield(p,efield_minus)
        
        # mu 
        delta_e = edft_minus-edft_plus
        if abs(delta_e) > p.conv_tol: # change
            dm_i_fd_tmp = (delta_e)*unit/(2*eps)
        else: 
            dm_i_fd_tmp = 0
        # alpha 
        alpha_ij_fd_tmp = (dm_plus - dm_minus)/(2*eps)/unit

        dm_i_fd[i] = dm_i_fd_tmp 
        alpha_ij_fd[i,:] = alpha_ij_fd_tmp
    
    # Results 
    # Dipole 
    dm = np.linalg.norm(dm_i)
    dm_fd = np.linalg.norm(dm_i_fd)
    print("mu_x mu_y mu_z abs(mu)")  
    print(f"analytical: {dm_i[0]: 10.5f} {dm_i[1]: 10.5f} {dm_i[2]: 10.5f} {dm: 10.5f}")
    print(f"numerical:  {dm_i_fd[0]: 10.5f} {dm_i_fd[1]: 10.5f} {dm_i_fd[2]: 10.5f} {dm_fd: 10.5f}")
    # Polarizeability 
    alpha_fd = np.trace(alpha_ij_fd)/3.
    print(f"analytical alpha_ij: {alpha_ij} \n alpha  : {alpha}")
    print(f"numerical  alpha_ij: {alpha_ij_fd} \n alpha  : {alpha_fd}")
    res = Results(key) 
    res.dm_i = dm_i
    res.dm = dm 
    res.dm_i_fd = dm_i_fd 
    res.dm_fd = dm_fd 
    res.alpha_ij = alpha_ij
    res.alpha = alpha 
    res.alpha_ij_fd = alpha_ij_fd
    res.alpha_fd = alpha_fd 
    return res 

def main(eps=0.0000001,tier_name="tier1"):
    """
        main 
        Main function to test the functionality of this routine. 
    """
    from dippo.db import CCCBDB, ChemSpider, PubChem, DFTopt

    # CCCBDB 
    cccbdb = Benchmark(name="CCCBDB",
                       systems_dct=CCCBDB,
                       kernel=calc,
                       eps=eps,
                       tier_name=tier_name) 
    cccbdb.kernel() 

    # ChemSpider 
    chemspider = Benchmark(name="ChemSpider",
                           systems_dct=ChemSpider,
                           kernel=calc,
                           eps=eps,
                           tier_name=tier_name)
    chemspider.kernel()

    # PubChem 
    pubchem = Benchmark(name="PubChem",
                        systems_dct=PubChem,
                        kernel=calc,
                        eps=eps,
                        tier_name=tier_name)
    pubchem.kernel() 

    # DFT optimized geometries 
    dftopt = Benchmark(name="DFTopt",
                       systems_dct=DFTopt,
                       kernel=calc,
                       eps=eps,
                       tier_name=tier_name)
    dftopt.kernel() 

    # Results
    print("\nResults\n")
    cccbdb._show()
    chemspider._show()
    pubchem._show()
    dftopt._show()


if __name__ == "__main__": 
    main(eps=0.1,tier_name="tier4")
    main(eps=0.01,tier_name="tier4")
    main(eps=0.001,tier_name="tier4")
    main(eps=0.0001,tier_name="tier4")
    main(eps=0.00001,tier_name="tier4")
    main(eps=0.000001,tier_name="tier4")
    main(eps=0.0000001,tier_name="tier4")
