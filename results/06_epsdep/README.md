- script to check 2-point finite-difference (FD) step size dependency 
- Note: 
    + only used in the revision process 
    + the values here are NOT used in manuscript 
    + different numerical parameter space was used 
