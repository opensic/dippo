from pyscf import gto, dft
from pyscf.tools import cubegen
from dippo import pkg_path

def calc_mep(f_name,spin=0,charge=0,basis='aug-pc-3',xc='LDA,PW'):
    """
        calc_mep
        Calculate molecular electrostatic potential (MEP). 
        Write the electronic density (den) and MEP as cube files. 
    """
    # Get the prefix 
    prefix = f_name.split("/")[-1].split('.')[0]
    # DFT 
    mol = gto.M(atom=f_name, basis=basis,spin=spin,charge=charge)
    mf = dft.UKS(mol)
    mf.verbose = 0
    mf.max_cycle = 300
    mf.conv_tol = 1e-8
    mf.xc = xc
    mf.grids.atom_grid = (200,1454)
    mf.kernel()
    mf.dip_moment()
    dm_dft = mf.make_rdm1()
    # electron density 
    cubegen.density(mol, f'{prefix}_den.cube', dm_dft[0]+dm_dft[1])
    # molecular electrostatic potential (MEP) 
    cubegen.mep(mol, f'{prefix}_mep.cube', dm_dft[0]+dm_dft[1])

def main():
    """
        main 
        Main function to test the functionality of the routine. 
    """
    sys_path = f"{pkg_path}/structures/CCCBDB/water_CCCBDB.xyz"
    calc_mep(sys_path)

if __name__ == '__main__':
    main()
