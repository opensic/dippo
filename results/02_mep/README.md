- Note: You need a Jmol installation. 
- This procedure produces the Fig. 2 of the manuscript. 
- run the PySCF calculation
  ```bash 
  python3 water_mep.py 
  ``` 
- visualize MEP with Jmol
  ```bash 
  jmol water_CCCBDB_mep.cube
  ``` 
- Draw MEP in Jmol
    + right click 
    + Select: Surfaces 
    + Select: Molecular Electrostatic Potential (range ALL)  
- Draw a dipole in Jmol
    + right click 
    + Select: Console 
    + type in the Jmol command line the following command 
    ```bash 
    dipole arrow1 {0 0 0.92771} {0 0 -0.92771} noCross width 0.15; color $arrow1 black
    ```
