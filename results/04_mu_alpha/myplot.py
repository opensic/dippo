import matplotlib
import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib.ticker as tick
from dippo.db import latex2key,key2latex
import dippo 

font = {'family' : 'sans',
        'weight' : 'normal',
        'size'   : 20}

matplotlib.rc('font', **font)
matplotlib.rc('lines', linewidth=3,markersize=12)

""" Plot and calculate errors for DFT dipole moments and polarizabilities (Fig.5 and Tab. 3) """

class Entry:
    """
        Entry class. 
    """
    def __init__(self,name): 
        self.name = name 
    def __str__(self): 
        print(self.name) 
        return f"{Entry(self.name)}" 

class Benchmark: 
    """
        Benchmark class. 
    """
    def __init__(self,name=None): 
        self.name = name 
    def __str__(self):
        s = '' 
        Keys = vars(self) 
        for key in Keys:
            s += key + "\n"
        return s 
    
def read_md(f_file,tier):
    """
        read_md 
        Read markdown file with Latex content for 
        one tier level. 
    """
    f = open(f_file,'r')
    ll = f.readlines() 
    f.close() 

    CCCBDB = Benchmark() 
    ChemSpider = Benchmark() 
    PubChem = Benchmark() 
    DFTopt = Benchmark() 
    for i,l in enumerate(ll):
        if l.find(tier) !=-1:
            for j in range(i+2,i+2+12):
                tmp = ll[j]
                tmp = tmp.replace("&","")
                tmp = tmp.split()
                name = latex2key[tmp[1]]
                db = tmp[0]
                e = Entry(tmp)
                e.dm = tmp[2]
                e.dm_fd = tmp[3]
                e.alpha = tmp[4]
                e.alpha_fd = tmp[5]

                if db == "CCCBDB": 
                    setattr(CCCBDB,name,e) 
                if db == "PubChem":
                    setattr(PubChem,name,e)
                if db == "ChemSpider":
                    setattr(ChemSpider,name,e)
                if db == "DFTopt":
                    setattr(DFTopt,name,e)

    bench = {"CCCBDB": CCCBDB, 
             "ChemSpider": ChemSpider,
             "PubChem": PubChem, 
             "DFTopt" : DFTopt}
    return bench

def read_all(f_file):
    """
        read_all 
        Read all data for all tier level. 
    """
    Tiers = ["aug-pc-0","aug-pc-1","aug-pc-2","aug-pc-3","aug-pc-4"]
    bench = {} 
    for tier in Tiers:
        db = read_md(f_file,tier)
        bench.update({tier: db})
    return bench 


def get_data(bench,db,sys,key): 
    """
        get_data
        Collect data/property for a coniguration state (db,sys,key). 
    """
    Tiers = ["aug-pc-0","aug-pc-1","aug-pc-2","aug-pc-3","aug-pc-4"]
    data = np.empty(len(Tiers),dtype=float)
    for i,tier in enumerate(Tiers):
        try: 
            db_entry = bench[tier][db]
            sys_entry = getattr(db_entry,sys)
            data[i] = getattr(sys_entry,key)
        except: print("Nothing",db,sys,key)
    return data 
    
def y_fmt(x, y):
    """
        y_fmt 
        Format for y axis ticks.
    """
    return f'{x:2.2f}'

def plot_nuclei_dataset(f_file = "results.md"): 
    """
        plot_nuclei_dataset
        Plot properties, i.e., mu and alpha, 
        for diffferent nuclei arragements/ databases. 
    """
    SYS = ["water","formaldehyde","nitromethane"]
    DB = ["CCCBDB","ChemSpider","PubChem","DFTopt"] 
    Tiers = ["aug-pc-0","aug-pc-1","aug-pc-2","aug-pc-3","aug-pc-4"]
    data = np.empty(len(Tiers),dtype=float)  
    bench = read_all(f_file) 
    mu_ref = {"water": 1.85,
              "formaldehyde": 2.33,
              "nitromethane": 3.46
    }
    alpha_ref = {"water": 10.13,
                 "formaldehyde": 18.69,
                 "nitromethane": 32.39
    }
    fig, axs = plt.subplots(2, 3)
    for i,sys in enumerate(SYS): 
        

        sub1, sub2 = axs[0, i], axs[1, i]
        sub1.set_title(key2latex[sys])

        prop = "dm"
        for db in DB:
            data = get_data(bench,db,sys,prop)
            sub1.plot(Tiers,data,
                    "-o",
                    label=f"{db}: {sys}, {prop}")
        sub1.plot(Tiers,len(Tiers)*[mu_ref[sys]],
                  color="black",
                  label="Ref.")
        sub1.set_xticks([])
        sub1.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))

        prop = "alpha"
        for db in DB:
            data = get_data(bench,db,sys,prop)
            sub2.plot(Tiers,data,
                    "-o",
                    label=f"{db}")
        sub2.plot(Tiers,len(Tiers)*[alpha_ref[sys]],
                  color="black",
                  label="Ref.")
        sub2.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))

    axs[0,0].set_ylabel(r"$\mu$ [D]")
    axs[1,0].set_ylabel(r"$\alpha$ [$a_{0}^{3}$]")

    axs[1,0].set_xticks(Tiers)
    axs[1,0].set_xticklabels(Tiers,rotation=9)
    axs[1,1].set_xticks(Tiers)
    axs[1,1].set_xticklabels(Tiers,rotation=9)
    axs[1,2].set_xticks(Tiers)
    axs[1,2].set_xticklabels(Tiers,rotation=9)

    sub2.legend(loc='upper center', 
                bbox_to_anchor=(-0.7, -0.10),
                fancybox=True, 
                shadow=True, 
                ncol=5,
                fontsize=20)

    plt.subplots_adjust(left=0.0695,
                        bottom=0.11, 
                        right=0.97, 
                        top=0.95, 
                        wspace=0.25, 
                        hspace=0.075)
    
    # Show the plot 
    plt.show()

def get_data2(bench,db,tier,key):
    """
        get_data2
        Collect data/property for a coniguration state (db,sys,key). 
    """
    SYS = ["water","formaldehyde","nitromethane"]
    data = np.empty(len(SYS),dtype=float)
    db_entry = bench[tier][db]
    for i,sys in enumerate(SYS):
        try:
            sys_entry = getattr(db_entry,sys)
            data[i] = getattr(sys_entry,key)
        except: print("Nothing",db,sys,key)
    return data



def get_errors(f_file = "results.md"):
    """
        get_errors
    """
    SYS = ["water","formaldehyde","nitromethane"]
    DB = ["CCCBDB","ChemSpider","PubChem","DFTopt"]
    Tiers = ["aug-pc-0","aug-pc-1","aug-pc-2","aug-pc-3","aug-pc-4"]
    data = np.empty(len(Tiers),dtype=float)
    bench = read_all(f_file)
    mu_ref = {"water": 1.85,
              "formaldehyde": 2.33,
              "nitromethane": 3.46
    }
    alpha_ref = {"water": 10.13,
                 "formaldehyde": 18.69,
                 "nitromethane": 32.39
    }

    data = np.empty((len(DB),len(Tiers)),dtype=float)
    for i,tier in enumerate(Tiers): 
        for j,db in enumerate(DB):
            # dm 
            data_dm = get_data2(bench,db,tier,"dm")
            data_dm_fd = get_data2(bench,db,tier,"dm_fd")
            dm_delta = np.array(data_dm) - np.array(data_dm_fd)
            dm_error = dm_delta.sum()/len(dm_delta) 
            # alpha 
            data_alpha = get_data2(bench,db,tier,"alpha")
            data_alpha_fd = get_data2(bench,db,tier,"alpha_fd")
            alpha_delta = np.array(data_alpha) - np.array(data_alpha_fd)
            alpha_error = alpha_delta.sum()/len(alpha_delta)
            data[j,i] = alpha_error 

    for i, db in enumerate(DB):
        s = f"{db} &"
        for j, tier in enumerate(Tiers): 
            s += f"{data[i,j]: 10.2f} &"
        print(s[:-2]+"\\ \\\\") 

def main():
    """
        main 
        Main function to test this routine.
    """
    f_file = "results.md"
    plot_nuclei_dataset(f_file=f_file) 
    get_errors(f_file=f_file)

if __name__ == "__main__": 
    main() 

