from copy import copy
from pyscf import scf
from pyflosic2.io.flosic_io import write_xyz
from pyflosic2.io.uflosic_io import read_flosic_xyz
from pyflosic2.time.timeit import tictoc
from pyflosic2.parameters.flosic_parameters import parameters, set_grid
from pyflosic2.sic.uflosic import UFLOSIC, ufodopt
import numpy

def run(f_xyz,grid=(200,1454),basis="aug-pc-2"):
    """
        run
        Run the FLO-SIC calculation.
        Note that the FODs are optimized, so the calculation can take a while.
    """
    # Standard parameters
    p = parameters(mode='unrestricted', log_name='UFLOSIC.log')

    # Computational parameter
    p.xc = 'LDA,PW'  # r2SCAN '497,498'
    p.verbose = 3
    p.conv_tol = 1e-8
    p.use_analytical_fforce = True
    p.opt_method = 'L-BFGS-B'
    p.basis = basis
    p.grid_level = grid

    # System information
    atoms, sym1, sym2 = read_flosic_xyz(f_xyz)
    p.init_atoms(atoms)

    p0 = copy(p)

    # DFT
    @tictoc(p)
    def dft(p):
        mf = scf.UKS(p.mol)
        # SS: b/c different logs
        # SS: PySCF has a different logger
        mf.verbose = 0
        mf.xc = p.xc
        mf.conv_tol = p.conv_tol
        mf = set_grid(mf,value=p.grid_level)
        edft = mf.kernel()
        dip = mf.dip_moment()
        print(dip)
        return p, mf, edft

    p, mf, edft = dft(p)
    mf0 = copy(mf)

    @tictoc(p)
    def flosic(p, mf):
        """
            Density matrix (DM) for Fermi orbital descriptors (FODs)
            Repeat outer and inner loop until DM is converged (SCF thresholds)
            and FODs are not changing (fmax).
            Tags: full-self-consistent (SCF) FLO-SIC, in-scf FLO-SIC

            outer loop: DM optimized for current FODs
            inner loop: FODs optimized for current DM

        """
        p.optimize_FODs = True
        mflo = UFLOSIC(mf=mf, p=p)
        etot = mflo.kernel()
        # SS: to be sure we do not influence other calcs
        del mflo
        return etot

    etot = flosic(p, mf)
    # SS: to see the real performance we do not want to use
    # SS: information from previous steps
    mf = copy(mf0)
    p = copy(p0)

    p.log.write('Basis :      {}'.format(p.basis))
    p.log.write('E(DFT) =     {}'.format(edft))
    p.log.write('E(FLO-SIC) = {}'.format(etot))


if __name__ == '__main__':
    run(f_xyz='initialFODs_LDQ.xyz',grid=(100,110),basis="STO3G")
