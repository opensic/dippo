- This folder contains FODs.
- For initial FODs, see initialFODs_*.xyz     
- For optimized FODs, see FODs_XC_Nrad_Nang_basis.xyz 
- The molecular geometry is the CCCBDB geometry

- To run the calculation, run
  ```bash
     python3 run.py
  ```

- To plot Fig.8 (a) 
  ```bash
     python3  barchart.py
  ```

