import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as tick

font = {'family' : 'sans',
        'weight' : 'normal',
        'size'   : 28}

matplotlib.rc('font', **font)
matplotlib.rc('lines', linewidth=3,markersize=12)

def y_fmt(x, y):
    """
        y_fmt 
        -----
        Used to format the y labels. 
    """
    return f'{x:2.2f}'

def read_data(f_file,basis_fam,n):
    """
        read_data
        Read data and return FMAX, E, and DM. 
    """
    f = open(f_file,"r")
    ll = f.readlines()
    f.close()
    FMAX = []
    E = []
    DM = []
    for i,l in enumerate(ll):
        if l.find(basis_fam+'-'+str(n)) != -1:
            e_name, fmax, dm, etot = l.split()
            fmax = float(fmax)
            etot = float(etot)
            dm = float(dm)
            FMAX.append(fmax)
            E.append(etot)
            DM.append(dm)
            print(e_name,fmax,etot,dm)
    E = np.array(E).copy()
    DM = np.array(DM).copy()
    FMAX = np.array(FMAX).copy()

    return FMAX, E, DM

def find_nearest(array, value):
    """
        find_nearest
        Find value in array which is closest to value. 
        Return the respective index idx. 
    """
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx


def bartplot(): 
    """
        batplot 
        barplot example

        Reference 
            - https://benalexkeen.com/bar-charts-in-matplotlib/
    """
    f_file = "data.dat"
    basis_fam = "aug-pc"
    n = [0,1,2,3,4]
    pc_n = [] 

    ind = np.arange(5)
    width = 0.15
    fig = plt.figure(1)
    axs = fig.subplots(2,1)

    for i,ni in enumerate(n):
        pc_ni = read_data(f_file=f_file,
                          basis_fam=basis_fam,
                          n=ni)
        pc_n.append(pc_ni)

    linit  = None
    lmed   = None
    lopt   = None
    fmaxtol = 5e-3
    for i,ni in enumerate(n):
        if i == 4:
            linit = 'initial'
            lmed  = 'loose'
            lopt  = 'tight'
        idx_med = find_nearest(pc_n[i][0],fmaxtol)
        axs[0].bar(ind[i]           , pc_n[i][1][0]         , width, color="tomato",      label=linit)
        axs[0].bar(ind[i] + width   , pc_n[i][1][idx_med]   , width, color="lightblue",   label=lmed)
        axs[0].bar(ind[i] + 2*width , pc_n[i][1][-1]        , width, color="darkseagreen",label=lopt)
        axs[1].bar(ind[i]           , pc_n[i][2][0]         , width, color="tomato",      label=linit)
        axs[1].bar(ind[i] + width   , pc_n[i][2][idx_med]   , width, color="lightblue",   label=lmed)
        axs[1].bar(ind[i] + 2*width , pc_n[i][2][-1]        , width, color="darkseagreen",label=lopt)

    axs[0].set_xticks([])
    axs[0].set_ylabel(r'$E_{\mathrm{tot}}$ [$E_{\mathrm{h}}$]')
    axs[0].legend()
    axs[0].set_ylim([-76.75,-76])

    axs[1].set_xticks(np.array(n)+1.0*width)
    axs[1].xaxis.set_ticklabels(['aug-pc-0','aug-pc-1','aug-pc-2','aug-pc-3','aug-pc-4'])
    axs[1].set_ylabel(r'$\mu$ [D]')


    axs[0].legend(loc='upper center',
                  bbox_to_anchor=(0.48, 1.35),
                  fancybox=True,
                  shadow=True,
                  ncol=3,
                  fontsize=40)


    plt.subplots_adjust(left=0.091,
                        bottom=0.1,
                        right=0.99,
                        top=0.89,
                        wspace=0.25,
                        hspace=0.075)
    plt.show()


def main(): 
    """
        main 
        ----
        Main function to test this routine. 
    """
    bartplot()


if __name__ == "__main__": 
    main() 
