from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(name='DipPo',
      version='0.0.0',
      description='PySCF/PyFLOSIC DIPoles and POlarizabilties',
      long_description=long_description,
      long_description_content_type="text/markdown",
      url='https://gitlab.com/opensic/dippo',
      author='Sebastian Schwalbe',
      author_email='theonov13@gmail.com',
      license='APACHE2.0',
      include_package_data=True,
      packages=find_packages(),
      zip_safe=False,
      install_requires=[
          'numpy',
      ]
      # Scripts: Ref.:
      # https://python-packaging.readthedocs.io/en/latest/command-line-scripts.html#the-scripts-keyword-argument
      #scripts=['scripts/pyflosic_dev'],
      #python_requires='>=3.6',
      )
