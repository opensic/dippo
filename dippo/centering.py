import numpy 
from pyflosic2.atoms.atoms import symbol2number
from pyflosic2.units.constants import atomic_masses

""" Utilities to center molecules using the moments of inertia """

def get_center_of_mass(atoms):
    """
        get_center_of_mass
        Get center of mass (COM). 
    """
    com = atoms.positions.sum(axis=0)/len(atoms.positions)
    return com 

def get_moments_of_inertia(atoms):
    """
        get_moments_of_inertia
        Get the inertia tensor. 
    """
    com = get_center_of_mass(atoms)
    positions = atoms.positions.copy() 
    positions -= com  
    masses = [atomic_masses[symbol2number[s]+1] for s in atoms.symbols]

    I = numpy.zeros((3,3),dtype=float)
    for (x,y,z), m in zip(atoms.positions,masses):

        I[0,0] += m * (y ** 2 + z ** 2)
        I[1,1] += m * (x ** 2 + z ** 2)
        I[2,2] += m * (x ** 2 + y ** 2)
        I[0,1] += -m * x * y
        I[1,0] += -m * x * y
        I[0,2] += -m * x * z
        I[2,0] += -m * x * z
        I[1,2] += -m * y * z
        I[2,1] += -m * y * z

    evals, evecs = numpy.linalg.eigh(I)
    return evals, evecs.transpose()

def center_by_moments_of_inertia(atoms): 
    """
        center_by_moments_of_inertia
        Center the atoms object by the inertia tensor I. 
    """
    _, I = get_moments_of_inertia(atoms)
    X = atoms.positions.copy() 
    X = numpy.dot(numpy.linalg.inv(I), X.T).T
    atoms.positions = X 
    return atoms 

def main():
    """
        main 
        Main function to test the functionality of this routine. 
    """
    from p02.db import water_ChemSpider 
    atoms = water_ChemSpider() 
    print(atoms.positions) 
    atoms = center_by_moments_of_inertia(atoms)
    print(atoms.positions)

if __name__ == "__main__": 
    main() 
