import numpy
from glob import glob 
from dippo import pkg_path 
from pyflosic2.io.flosic_io import read_xyz

def gen_systems_from_xyz(f_name, charge=0, spin=0):
    """
        Generate: System information from xyz files
        -------------------------------------------
        Powered by lazyness.
    """
    # SS: Adjusted name as it is a path name 
    name = f_name.split("/")[-1] 
    name = name.split('.')[0]
    print('def {}():'.format(name))
    print('    """')
    print('        {} example'.format(name))
    print('    """')
    sym, pos, sym_fod1, sym_fod2 = read_xyz(f_name)
    # SS: may this is not really needed 
    if sym_fod1 is None: 
        sym_fod1 = "X"
    if sym_fod2 is None: 
        sym_fod2 = "He"

    values, index, counts = numpy.unique(sym, return_counts=True, return_index=True)
    # Note: numpy.unique sorts everything internally
    # We need to reconstruct the correct order.
    idx = index.argsort()
    values = values[idx]
    counts = counts[idx]
    s = ''
    for v, w in zip(values, counts):
        s += "['{}']*{}+".format(v, w)
    s = s[:-1]
    print("    sym = {}".format(s))
    ptot = '['
    for i, p in enumerate(pos):
        print('    p{} = [{},{},{}]'.format(i, p[0], p[1], p[2]))
        ptot += "p{},".format(i)
    ptot = ptot[:-1] + ']'
    print('    pos = {}'.format(ptot))
    print('    charge = {}'.format(charge))
    print('    spin = {}'.format(spin))
    print("    atoms = Atoms(sym,pos,charge=charge,spin=spin,elec_symbols=['{}','{}'])".format(sym_fod1, sym_fod2))
    print("    return atoms")

def main():
    """
        main 
        Main function to test this routine.
    """
    sys_path = f"{pkg_path}/structures"
    # databases as groups 
    DB = ["CCCBDB","ChemSpider","PubChem","DFTopt"] 
    for db in DB: 
        db_path = f"{sys_path}/{db}"
        XYZ = glob(f"{db_path}/*.xyz")
        for xyz in XYZ:
            gen_systems_from_xyz(xyz) 

if __name__ == "__main__": 
    main() 
