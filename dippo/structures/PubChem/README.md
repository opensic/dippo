- water 
    + https://pubchem.ncbi.nlm.nih.gov/compound/962
    + 2D structure 
    + babel --sdf Structure2D_CID_962.sdf --xyz water_PubChem.xyz 
- formalaldehye 
    + 3D structure 
    + https://pubchem.ncbi.nlm.nih.gov/compound/712#section=3D-Conformer
    + babel --sdf Conformer3D_CID_712.sdf --xyz formaldehyde_PubChem.xyz
- nitromethane 
    + 3D structure 
    + https://pubchem.ncbi.nlm.nih.gov/compound/6375#section=3D-Conformer
    + babel --sdf Conformer3D_CID_6375.sdf --xyz nitromethane_PubChem.xyz 
