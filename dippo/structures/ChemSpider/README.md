- note use 3D representation 
- H2O 
    + http://www.chemspider.com/Chemical-Structure.937.html
    +  babel --mol 937.mol --xyz h2o_ChemSpider.xyz 
- formaldehyde
    + alias: formyl 
    + http://www.chemspider.com/Chemical-Structure.692.html
    + babel --mol 692.mol --xyz formaldehyde_ChemSpider.xyz 

- nitromethane 
    + http://www.chemspider.com/Chemical-Structure.6135.html
    + babel --mol 6135.mol --xyz nitromethane_ChemSpider.xyz 

