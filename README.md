![GitLab Logo](/doc/images/dippo_logo_v2.png)    

# DipPo
**(Dip)oles and (Po)larizabilities**    

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6246152.svg)](https://doi.org/10.5281/zenodo.6246152)


Data and scripts used for the following article    

* **Effect of molecular and electronic geometries on the electronic density in FLO-SIC**     
   S. Liebing, K. Trepte, and S. Schwalbe    
   arXiv e-prints, Subject: Chemical Physics (physics.chem-ph); Computational Physics (physics.comp-ph), 2022, [arXiv:2201.11648](https://arxiv.org/abs/2201.11648)    
   [Supplemental Material](supplemental/supp.pdf) 

Note that the manuscript was reviewed.    
Thus, the content, data and scripts      
for the project have changed accordingly.      
    
# Authors 

S. Liebing  (SiL, science@liebing.cc)   
K. Trepte   (KT, kai.trepte1987@gmail.com)   
S. Schwalbe (SS, theonov13@gmail.com)     

# Installation 

```bash 
pip3 install -e .
```

# Functionality 

| What?                          | Description                                                           	 |
| :----------------------------: | :---------------------------------------------------------------------------: |
| dippo/supplemental             | Supplemental material                                                  	 | 
| dippo/dippo/structures         | all nuclei starting geometries sorted in subdirectories                	 |  
| dippo/results/01_dft_overview  | generate Fig. 1 of the manuscript                                      	 | 
| dippo/results/02_mep           | generate Fig. 2 of the manuscript                                      	 | 
| dippo/results/03_conv          | grid and basis set convergence for DFT and FLO-SIC                     	 |
| dippo/results/04_mu_alpha      | influence of molecular geometry for DFT (mu, alpha), Fig. 5 and Tab. 3 	 |
| dippo/results/05_fodopt        | initial and optimized FOD geometries, used for Fig. 6, Fig. 7, Fig. 8, Tab. 4 |
| dippo/results/06_epsdep        | epsilon dependency (revision only)                                        |    

